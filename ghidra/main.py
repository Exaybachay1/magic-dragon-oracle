import logging
import os
import subprocess
import sys
from jfx_bridge import bridge

import ghidra


def lookup_symbol(address):
    return lookup_symbol_by_address(toAddr(address))


def lookup_symbol_by_address(address):
    symbol = getSymbolAt(address)
    if symbol is None:
        return None, None, None, None

    symbol_name = symbol.getName(True)
    symbol_address = symbol.getAddress()

    func = getFunctionAt(address)

    func_name = str(func) if func is not None else None
    func_entry_point = func.getEntryPoint() if func is not None else None
    func_signature = func.getSignature() if func is not None else None

    return symbol_name, symbol_address, func_name, func_entry_point, func_signature


def find_symbols(query):
    def get_program_location_for_symbol(symbol):
        return symbol.getProgramLocation()

    def get_valid_symbol_locations_for_program(program, input_string, namespace):
        symTable = program.getSymbolTable()
        locations = map(lambda s: get_program_location_for_symbol(s), symTable.getSymbols(input_string, namespace))
        return locations

    def process_symbol_in_programs(query):
        # here was once a loop over all programs. We only have one program. The current one.
        return get_valid_symbol_locations_for_program(currentProgram, query, currentProgram.getGlobalNamespace())

    def process_symbol_in_parsed_scope(query):
        symbol_table = currentProgram.getSymbolTable()

        def get_namespace_from_scopes(scopes):
            parent_namespace = currentProgram.getGlobalNamespace()
            for scope_name in scopes:
                parent_namespace = symbol_table.getNamespace(scope_name, parent_namespace)

            return parent_namespace

        if "::" not in query:
            return None

        parts = query.split('::')
        scopes = parts[:-1]
        symbol_name = parts[-1:][0]

        namespace = get_namespace_from_scopes(scopes)

        return get_valid_symbol_locations_for_program(currentProgram, symbol_name, namespace)

    def transform(item):
        symbol_name, symbol_address, func_name, func_entry_point, func_signature = lookup_symbol_by_address(
            item.getAddress())
        return (symbol_name, symbol_address)

    return map(lambda v: transform(v), process_symbol_in_programs(query) or process_symbol_in_parsed_scope(query))


def run_server(server_host, server_port, response_timeout=bridge.DEFAULT_RESPONSE_TIMEOUT):
    """ Run a ghidra_bridge_server (forever)
        server_host - what address the server should listen on
        server_port - what port the server should listen on
        response_timeout - default timeout in seconds before a response is treated as "failed"
        background - false to run the server in this thread (script popup will stay), true for a new thread (script popup disappears)
    """
    server = bridge.BridgeServer(server_host=server_host,
                                 server_port=server_port, loglevel=logging.INFO, response_timeout=response_timeout)

    server.run()


if __name__ == '__main__':
    host = os.getenv('SERVER_HOST') or bridge.DEFAULT_HOST
    port = os.getenv('SERVER_PORT') or 4768
    run_server(host, port)
