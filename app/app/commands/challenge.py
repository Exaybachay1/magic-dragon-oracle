import logging

import discord
import app.config
from app.client import client
from discord import ui, User, Member
from typing import Union
import re
from enum import Enum
from burtle_random import BurtleRandom

logger = logging.getLogger("commands.challenge")


class Reason(Enum):
    OK = 0
    PARAM = 1
    REGEX = 2
    FLAG1 = 3
    FLAG2 = 4
    FAIL = 5


def check_challenge(answer: str, user: Union[User, Member]) -> (bool, Reason):
    logger.info(f"Answer {answer}")
    logger.info(f"User {user}")
    try:
        if answer is None:
            return False, Reason.PARAM

        if user is None:
            return False, Reason.PARAM

        res = re.match(r'^FLAG\{(\d{1,32})\.(\w{1,16})}$', answer)
        if res is None:
            return False, Reason.REGEX

        ran = BurtleRandom(int(res.group(1)))
        actual = ran.randint(0, 9999)

        logger.info(f"Expected {user.discriminator}")
        logger.info(f"Actual {actual}")

        if not actual == int(user.discriminator):
            return False, Reason.FLAG1

        sum1 = sum(ord(c) for c in user.name)
        sum2 = sum(ord(c) for c in res.group(2))

        logger.info(f"Sum1 {sum1}")
        logger.info(f"Sum2 {sum2}")

        if not (sum1 + sum2) % 1337 == 0:
            return False, Reason.FLAG2

        return True, Reason.OK
    except Exception as ex:  # I'M SORRY PEP 8: E772
        logger.error(str(ex))
        return False, Reason.FAIL


class Questionnaire(ui.Modal, title='The dragons challenge'):
    answer = ui.TextInput(
        label='Answer',
        placeholder='Tell me the answer.',
        min_length=6,
        max_length=64
    )

    async def on_submit(self, interaction: discord.Interaction):
        result, reason = check_challenge(self.answer.value, interaction.user)
        logging.info("Result", result)
        logging.info("Reason", reason)
        if result:
            role = interaction.guild.get_role(app.config.DRAGON_ROLE_ID)
            if role is None:
                logger.error(f"Role for {app.config.DRAGON_ROLE_ID} is None")

            await interaction.user.add_roles(role, reason=f"Answer '{self.answer.value}' was correct")
            await interaction.response.send_message(
                f"Unbelievable! That's correct. {interaction.user.name} is now part of the dragons crew")
        else:
            messages = {
                Reason.PARAM: "The dragon does not even notice you.",
                Reason.REGEX: "The dragon doesn't seem to happy about that.",
                Reason.FLAG1: "The dragon looks at you, then looks away.",
                Reason.FLAG2: "The dragon glares at you.",
                Reason.FAIL: "The dragon is shaking it's head.",
            }
            await interaction.response.send_message(
                messages[reason],
                ephemeral=True)


@client.tree.command()
async def challenge(interaction: discord.Interaction):
    """
    Are you ready?
    """
    await interaction.response.send_modal(Questionnaire())
