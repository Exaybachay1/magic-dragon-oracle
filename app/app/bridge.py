import ghidra_bridge
from typing import Optional, List
from app.config import GHIDRA_HOST
import jfx_bridge


def is_address_expression(input_string: str):
    return any(c in input_string for c in ['+', '-', '*'])


def lookup_symbol(address: str):
    ...


def lookup_symbol_by_address(address):
    ...


def find_symbols(input_string: str):
    ...


bridge = ghidra_bridge.GhidraBridge(connect_to_host=GHIDRA_HOST, namespace=globals())
