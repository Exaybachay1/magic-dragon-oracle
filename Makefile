GHIDRA_URL := "https://github.com/NationalSecurityAgency/ghidra/releases/download/Ghidra_10.2_build/ghidra_10.2_PUBLIC_20221101.zip"
GHIDRA_FOLDER := "ghidra_10.2_PUBLIC"
GHIDRA_TAG_VERSION := 10.2.1

openjdk: docker/openjdk/
	docker build -t florian0/openjdk:17-headless $<
	docker push florian0/openjdk:17-headless

ghidra-headless: docker/ghidra-headless/ openjdk
	docker build --build-arg URL=${GHIDRA_URL} --build-arg FOLDER=${GHIDRA_FOLDER} -t florian0/ghidra-headless:${GHIDRA_TAG_VERSION} $<
	docker push florian0/ghidra-headless:${GHIDRA_TAG_VERSION}
